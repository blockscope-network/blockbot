# 🔬 uScope

An Umee monitoring Telegram bot to alert you from your validator becoming a zombie.
<br/><br/>

## 🗒 Description

uScope is a monitoring and alerting tool for Umee validator nodes, it aims to be simple to install and use, and useful in features and information provided.

uScope displays your node's info in a friendly way, via your favourite communication channel: Telegram and the shell.

## 💾 Installation

uScope is written in Go Lang as most Umee tools, so to make it work just download the precompiled binary or clone the repo and compile the source code.

### **Install Go Lang**

TODO

### **Install dependencies**

```sh
sudo apt install build-essentials curl git -y
```

### **Clone the repository**

```sh
cd ~
git clone https://gitlab.com/blockscope-network/uscope.git
```

### **Compile source code**

```sh
cd ./uscope/
go build
```

## 🏃 Run it

To run uScope you can just execute the binary or create a service unit to get it always running (check next section).

```sh
./uscope
```

## 😈 Run with a Service

As a monitoring tool it's better to handle its execution with a Linux service, so it starts on boot/reboots and restarts in the case it exits the execution.

### **Create service file**

```sh
UMEE_PATH=$(dirname "$(which umeed)")

echo "[Unit]
Description=uScope Service

[Service]
User="$USER"
Group="$USER"
Environment=PATH=$PATH:$UMEE_PATH
ExecStart="$PWD/uscope"
Restart=on-failure
RestartSec=5s

[Install]
WantedBy=multi-user.target " > uscope.service
```

### **Move service file to services location**

```sh
sudo mv ./uscope.service /etc/systemd/system/uscope.service
```

### **Enable the service**

```sh
sudo systemctl daemon-reload
sudo systemctl enable uscope
```

### **Run the service**

```sh
sudo systemctl start uscope
```

### **Check logs / output**

```sh
sudo journalctl -u uscope -fo cat
```

### **Stop the service**

```sh
sudo systemctl stop uscope
```

### **Check service status**

```sh
sudo systemctl status uscope
```

## 🏓 Usage

As a monitoring/alerting tool the main thing in uScope is sending you alerts of important events on your validator node. Here is a description on how the info messages are structured and the meaning of its parts.

### **Messages**

TODO
A uScope message consists of four parts in this order:

- Category: an emoji indicating the type of the message
- Section: an emoji indicating to what the alert belongs to
- Content: a meaningful message depicting the event and related info
- Id: an emoji assigned to each message

### **Categories**

Are represented with a coloured ball emoji representing a type of message.

- `🔵 INFO: messages that just communicate interesting info`
- `🟠 WARN: messages that alert from something important that could indicate some issue`
- `🔴 ERROR: messages that alert from a problem that is critical`
- `🟢 OK: messages that communicate that a WARN or an ERROR has been reverted`

### **Sections**

Are represented with an emoji related to the part of the system that the alert regards to.

- `📺 Node: messages regarding your node or the network cluster (not syncing, missing blocks, version events)`
- `🎼 Peggo: messages regarding your node or the network cluster (not syncing, missing blocks, version events)`
- `🕵️ RPC: messages regarding the RPC communication (offline rpc)`
- `🍖 Balance: messages regarding the balance of the voting account (low balance)`
- `🏓 Ping: messages sent periodically, with some generic info (vote balance, epoch progress, slots produced)`
- `🤿 Stake: messajaiges regarding changes in validator stake`

### **Id**

Each message is assigned a unique emoji for visual purposes, so once you know the messages you can identify which one you're reading by just looking at the emoji.

## 👨‍🔧 Configuration

### **Config**

There are some variables at the begining of the script that you need set in order for uScope to work

TODO

### **Customize**

Below the configuration variables you can find some others that you can tweak in order to adjust uScope behaviour to your needs. Time intervals are specified in seconds, token amounts in uUmee (10e6 uToken == 1 Token).

```sh
INTERVAL=10                         # Time interval to query the RPC and detect alerts / events
PING_INTERVAL=10800                 # Time interval for sending a Ping message
ALERT_REPEAT_INTERVAL=300           # Time interval for re-sending important alerts if the issue hasn't been reverted
ALERT_TIME_THRESHOLD=60             # Time threshold for an alert to be sent (ignore intermitent issues)
STAKE_THRESHOLD=10000000000         # Amount threshold for a stake change to considered as an alert (ignore small changes)
BALANCE_THRESHOLD=10000000000       # Vote account minimum balance threshold for an alert to be sent
MISSED_BLOCKS_THRESHOLD=0.05         # Missed slots fraction threshold for sending alerts (Example: 0.15 equals 15%)
```

### **Telegram**

Besides showing logs in the shell terminal uScope communicate with your own Telegram bot, for that you'll need to set up your Telegram bot. You can find tons of resources for doing that.

For Telegram set your bot id and the chat id of your telegram program with the bot.

```sh
TG_BOT_ID="SET_YOUR_BOT_ID"
TG_CHAT_ID="SET_YOUR_CHAT_ID"
```

## 👀 Visuals

TODO
Telegram screenshot

## 💡 Troubleshooting

TODO

## 🛣 Roadmap

Release Alpha (15th July)

- MVP Delevopment
- Advanced features
- Configuration development
- Telegram integration
- Final testing (testnet and mainnet)
- Improvements

Release Beta (31th August)

- Bug fixes
- UX, formatting & documentation
- System service
- Publish release
- Communication (Discord, Telegram channels)
- Feedback
- Fixes
- Improvements

## 🔢 Versions

```
v0.0.1-alpha - 15/07/2022
v0.0.1-beta - 31/08/2022
```

## 📬 Support

Contact us throught Gitlab or via email at blockscope@protonmail.com

## 🛠 Contributing

Feel free to contribute new features by creating a Pull Request in this repository. Get in contact with us for any doubt on how to develop before submitting the PR to avoid wasting development time.

## ✅ Project status

Alive! We keep updating the project with bug fixes and improvements, don't hesitate to submit an issue to the repository.

## 🦠 Authors and acknowledgment

Developed by Blockscope for the Umee project and the Umee validators DAO and community.

If you use and like uScope, feel free to:

- delegate UMEE with our mainnet validator
- donate some UMEE to buy some coffee ☕ and food 🧬 to feed the web3 cell culture 🔬

```
Delegations: umeevaloper1ux8apnt5unupg7r3f2cdxh8smgpun4479j5rav
Donations: umee1ux8apnt5unupg7r3f2cdxh8smgpun4479knvvx
```

Thanks for your support! 🙏

## 📝 License

This project is subject to the GNU General Public License v3 (GPL-3)
[GNU GPL License](<https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3)>).
