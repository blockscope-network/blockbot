module blockbot

go 1.16

require (
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible // indirect
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	github.com/tidwall/gjson v1.8.0 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	gopkg.in/tucnak/telebot.v2 v2.3.5 // indirect
)
